import { NgModule } from "@angular/core";
import { MatListModule } from "@angular/material/list";
import { BrowserModule } from "@angular/platform-browser";
import { SourceComponent } from './source.component';

@NgModule({
  declarations: [SourceComponent],
  imports: [
    BrowserModule,
    MatListModule
  ],
  exports: [SourceComponent],
  bootstrap: [SourceComponent]
})
export class SourceModule {}
