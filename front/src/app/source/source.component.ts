import { Component, EventEmitter, Input, Output } from "@angular/core";
import { MatSelectionListChange } from "@angular/material/list";
import { Source } from "../entities/source";

@Component({
  selector: "app-source",
  templateUrl: "./source.component.html",
  styleUrls: ["./source.component.scss"]
})
export class SourceComponent {
  @Input() sources: Source[];
  @Output() selectedSources: EventEmitter<Source[]> = new EventEmitter();

  public onSelectionChange(event: MatSelectionListChange) {
    this.selectedSources.emit(
      event.source.selectedOptions.selected.map(select => select.value)
    );
  }
}
