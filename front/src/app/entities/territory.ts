export class Territory {
  public subTerritories: Territory[];

  public constructor(
    public id: number,
    public code: string,
    public name: string,
    public kind: string,
    public parent_id: number
  ) {}
}
