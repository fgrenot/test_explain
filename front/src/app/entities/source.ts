export interface Source {
  id: number;
  code: string;
  name: string;
  kind: string;
}
