import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "./../environments/environment";
import { Source } from "./entities/source";
import { Territory } from "./entities/territory";
import { Theme } from "./entities/theme";

@Injectable({
  providedIn: "root"
})
export class AppService {
  private readonly baseUrl = environment.baseUrl;

  public constructor(private http: HttpClient) {}

  public sources(): Observable<Source[]> {
    return this.http.get<Source[]>(this.baseUrl + "sources/");
  }

  public themes(): Observable<Theme[]> {
    return this.http.get<Theme[]>(this.baseUrl + "themes/");
  }

  public territories(id: number = undefined): Observable<Territory[]> {
    return this.http.get<Territory[]>(
      this.baseUrl + "territories/" + (id ? id : "")
    );
  }
}
