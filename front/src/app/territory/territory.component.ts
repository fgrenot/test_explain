import {
  CollectionViewer,
  SelectionChange,
  SelectionModel
} from "@angular/cdk/collections";
import { FlatTreeControl } from "@angular/cdk/tree";
import { Component, Injectable, OnInit, Output, EventEmitter } from "@angular/core";
import { BehaviorSubject, merge, Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { AppService } from "../app.service";
import { Territory } from "../entities/territory";

/** Flat node with expandable and level information */
export class DynamicFlatNode {
  public isLoading = false;
  constructor(
    public id: number,
    public item: string,
    public level = 1,
    public expandable = true
  ) {}
}

/**
 * Database for dynamic data. When expanding a node in the tree, the data source will need to fetch
 * the descendants data from the database.
 */
export class DynamicDatabase {
  private dataMap = new Map<number, Territory>();
  private flatDataMap = new Map<number, Territory>();
  private service: AppService;

  constructor() {}

  /** Initial data from database */
  initialData(service: AppService): Observable<DynamicFlatNode[]> {
    this.service = service;
    return this.service.territories().pipe(
      map(territories => {
        let datas: DynamicFlatNode[] = [];
        territories.forEach(territory => {
          this.dataMap.set(territory.id, territory);
          this.flatDataMap.set(territory.id, territory);
          datas.push(
            new DynamicFlatNode(territory.id, territory.name, 1, true)
          );
        });
        return datas;
      })
    );
  }

  children(nodeId: number): Observable<Territory[]> {
    let result = this.dataMap.get(nodeId).subTerritories;
    if (!result) {
      return this.service.territories(nodeId).pipe(
        map(territories => {
          this.dataMap.get(nodeId).subTerritories = territories;
          territories.forEach(territory => {
            this.dataMap.set(territory.id, territory);
            this.flatDataMap.set(territory.id, territory);
          });
          return territories;
        })
      );
    }
    return of(result);
  }

  node(nodeId: number): Territory {
    return this.flatDataMap.get(nodeId);
  }

  isExpandable(nodeId: number): boolean {
    return this.dataMap.has(nodeId);
  }

  unexpandable(nodeId: number) {
    this.dataMap.delete(nodeId);
  }
}

/**
 * File database, it can build a tree structured Json object from string.
 * Each node in Json object represents a file or a directory. For a file, it has filename and type.
 * For a directory, it has filename and children (a list of files or directories).
 * The input will be a json object string, and the output is a list of `FileNode` with nested
 * structure.
 */
@Injectable()
export class DynamicDataSource {
  dataChange = new BehaviorSubject<DynamicFlatNode[]>([]);

  constructor(
    private treeControl: FlatTreeControl<DynamicFlatNode>,
    private database: DynamicDatabase
  ) {}

  get data(): DynamicFlatNode[] {
    return this.dataChange.value;
  }

  set data(value: DynamicFlatNode[]) {
    this.treeControl.dataNodes = value;
    this.dataChange.next(value);
  }

  connect(collectionViewer: CollectionViewer): Observable<DynamicFlatNode[]> {
    this.treeControl.expansionModel.onChange.subscribe(change => {
      if (change.added || change.removed) {
        this.handleTreeControl(change);
      }
    });

    return merge(collectionViewer.viewChange, this.dataChange).pipe(
      map(() => this.data)
    );
  }

  /** Handle expand/collapse behaviors */
  handleTreeControl(change: SelectionChange<DynamicFlatNode>) {
    if (change.added) {
      change.added.forEach(node => this.toggleNode(node, true));
    }
    if (change.removed) {
      change.removed
        .slice()
        .reverse()
        .forEach(node => this.toggleNode(node, false));
    }
  }

  /** Toggle the node, remove from display list */
  toggleNode(node: DynamicFlatNode, expand: boolean) {
    const index = this.data.indexOf(node);
    if (index < 0) {
      // If no children, or cannot find the node, no op
      return;
    }
    node.isLoading = true;
    this.database.children(node.id).subscribe(children => {
      if (!children || children.length == 0) {
        this.database.unexpandable(node.id);
        if (index >= 0) {
          this.data.splice(
            index,
            1,
            new DynamicFlatNode(node.id, node.item, node.level, false)
          );
        }
      } else if (expand) {
        const nodes = children.map(
          territory =>
            new DynamicFlatNode(
              territory.id,
              territory.name,
              node.level + 1,
              this.database.isExpandable(territory.id)
            )
        );
        this.data.splice(index + 1, 0, ...nodes);
      } else {
        let count = 0;
        for (
          let i = index + 1;
          i < this.data.length && this.data[i].level > node.level;
          i++, count++
        ) {}
        this.data.splice(index + 1, count);
      }

      // notify the change
      this.dataChange.next(this.data);
      node.isLoading = false;
    });
  }
}

@Component({
  selector: "app-territory",
  templateUrl: "./territory.component.html",
  styleUrls: ["./territory.component.scss"],
  providers: [DynamicDatabase]
})
export class TerritoryComponent implements OnInit {
  public treeControl: FlatTreeControl<DynamicFlatNode>;
  public dataSource: DynamicDataSource;
  public selection = new SelectionModel<DynamicFlatNode>(true);
  @Output() public selectedTerritories: EventEmitter<Territory[]> = new EventEmitter<Territory[]>();
  @Output() public selectedTerritoriesDatas: Territory[] = [];

  public constructor(
    private database: DynamicDatabase,
    private service: AppService
  ) {
    this.treeControl = new FlatTreeControl<DynamicFlatNode>(
      this.getLevel,
      this.isExpandable
    );
    this.dataSource = new DynamicDataSource(this.treeControl, database);
  }

  public ngOnInit() {
    this.database
      .initialData(this.service)
      .subscribe(datas => (this.dataSource.data = datas));
  }

  public getLevel = (node: DynamicFlatNode) => node.level;

  public isExpandable = (node: DynamicFlatNode) => node.expandable;

  public hasChild = (_: number, node: DynamicFlatNode) => node.expandable;

  public itemSelectionToggle(node: DynamicFlatNode): void {
    this.selection.toggle(node);
    this.updateSelectedTerritories(node);
  }

  private updateSelectedTerritories(node: DynamicFlatNode) {
    const territory = this.database.node(node.id);
    if (this.selection.isSelected(node)) {
      this.selectedTerritoriesDatas.push(territory);
    } else {
      this.selectedTerritoriesDatas.splice(this.selectedTerritoriesDatas.indexOf(territory), 1);
    }
    this.selectedTerritories.emit(this.selectedTerritoriesDatas);
  }
}
