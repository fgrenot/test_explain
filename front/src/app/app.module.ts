import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { AppService } from "./app.service";
import { TerritoryModule } from "./territory/territory.module";
import { SourceModule } from './source/source.module';
import { ThemeModule } from './theme/theme.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    TerritoryModule,
    SourceModule,
    ThemeModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule {}
