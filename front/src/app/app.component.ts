import { Component, OnInit } from "@angular/core";
import { AppService } from "./app.service";
import { Source } from "./entities/source";
import { Territory } from "./entities/territory";
import { Theme } from "./entities/theme";

export interface Configuration {
  project_code: string;
  territories: string[];
  themes: string[];
  sources: string[];
}

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  public sources: Source[];
  public themes: Theme[];

  public projectCode: string;
  public selectedSources: Source[];
  public selectedTerritories: Territory[];
  public selectedThemes: Theme[];

  public constructor(private service: AppService) {}

  public ngOnInit() {
    this.service.sources().subscribe(sources => (this.sources = sources));
    this.service.themes().subscribe(themes => (this.themes = themes));
  }

  public canGenerateJson(): boolean {
    if (!this.projectCode || this.projectCode.length == 0 || !/^[-a-zA-Z0-9\-_]+$/g.test(this.projectCode)) {
      return false;
    }
    if (!this.selectedTerritories || this.selectedTerritories.length == 0) {
      return false;
    }
    if (!this.selectedSources || this.selectedSources.length == 0) {
      return false;
    }
    if (!this.selectedThemes || this.selectedThemes.length == 0) {
      return false;
    }
    return true;
  }

  public generateJson() {
    if (!this.canGenerateJson()) {
      return;
    }
    const result: Configuration = {
      project_code: this.projectCode,
      territories: this.selectedTerritories.map(territory => territory.code),
      themes: this.selectedThemes.map(theme => theme.name),
      sources: this.selectedSources.map(source => source.code)
    };
    this.downloadJson(result);
  }

  private downloadJson(configuration: Configuration) {
    const element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:text/json;charset=UTF-8," +
        encodeURIComponent(JSON.stringify(configuration))
    );
    element.setAttribute("download", `${configuration.project_code}_configuration.json`);
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }
}
