import { NgModule } from "@angular/core";
import { MatListModule } from "@angular/material/list";
import { BrowserModule } from "@angular/platform-browser";
import { ThemeComponent } from "./theme.component";

@NgModule({
  declarations: [ThemeComponent],
  imports: [
    BrowserModule,
    MatListModule
  ],
  exports: [ThemeComponent],
  bootstrap: [ThemeComponent]
})
export class ThemeModule {}
