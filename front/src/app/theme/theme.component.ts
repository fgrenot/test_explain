import { Component, EventEmitter, Input, Output } from "@angular/core";
import { MatSelectionListChange } from "@angular/material/list";
import { Theme } from "../entities/theme";

@Component({
  selector: "app-theme",
  templateUrl: "./theme.component.html",
  styleUrls: ["./theme.component.scss"]
})
export class ThemeComponent {
  @Input() themes: Theme[];
  @Output() selectedThemes: EventEmitter<Theme[]> = new EventEmitter();

  public onSelectionChange(event: MatSelectionListChange) {
    this.selectedThemes.emit(
      event.source.selectedOptions.selected.map(select => select.value)
    );
  }
}
