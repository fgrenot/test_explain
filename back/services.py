from db_queries import selectAll

from flask import Flask, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/themes/')
def allThemes():
    return jsonify(selectAll(table='themes', filter=None, keys=['id', 'name']))

@app.route('/sources/')
def allSources():
    return jsonify(selectAll(table='sources', filter=None, keys=['id', 'code', 'name', 'kind']))

@app.route('/territories/')
@app.route('/territories/<int:parent_id>')
def allTerritories(parent_id=None):
    return jsonify(selectAll(table='territories', filter={'parent_id': parent_id}, keys=['id', 'parent_id', 'code', 'name', 'kind']))
