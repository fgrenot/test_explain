import psycopg2

from db_config import config

def connect(section):
    """
    Gets connection to PostgreSQL database instance
    :param section: section of database configuration file to use
    :return: db connection
    """

    try:
        params = config(section=section)
        conn = psycopg2.connect(**params)
        print('Connection to database created')
        return conn
    except (Exception, psycopg2.DatabaseError) as err:
        print(connect.__name__, err)
        exit(1)

def close(conn):
    """
    Closes database connection
    """
    if conn is not None:
        conn.close()
        print('Connection to database closed')
