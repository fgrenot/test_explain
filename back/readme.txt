required:
py -m pip install virtualenv

execute:
py -m virtualenv venv
cd back
py -m virtualenv venv
venv\Scripts\activate
py -m pip install --upgrade -r script/requirements.txt
set FLASK_APP=services.py
py -m flask run