import psycopg2

from db_connector import connect, close

def selectAll(table, filter={}, keys=[]):
    try:
        conn = connect('development')
        with conn:
            with conn.cursor() as curs:
                request = "SELECT * FROM " + table
                if filter is not None:
                    first = True
                    for key in filter.keys():
                        if first:
                            first = False
                            request += " WHERE "
                        else:
                            request += " AND "
                        if filter[key] is None:
                            request += key + " is null"
                        else:
                            request += key + " = " + str(filter[key])
                curs.execute(request)
                results = []
                row = curs.fetchone()
                while row is not None:
                    results.append(toObject(row, keys))
                    row = curs.fetchone()
                return results
    except (psycopg2.OperationalError, psycopg2.DatabaseError) as err:
        print(selectAll.__name__, err)
    finally:
        close(conn)

def toObject(result, keys:[]):
    out = {}
    for key in keys:
        out['' + key] = result[keys.index(key)]
    return out